#!/bin/bash
TARGET=android-24
ARCH=arch-arm64
export NDK_ROOT=/Users/musictom/Library/Android/sdk/ndk-bundle

SYSRT=$NDK_ROOT/platforms/$TARGET/$ARCH
TOOL=aarch64-linux-android
MYHOME=/Users/musictom/libs/android/$ARCH/usr
INCLUDEPTH=$MYHOME/include
LIBPTH=$MYHOME/lib
#PLATFORMS=(arm64-v8a x86_64 mips64 armeabi armeabi-v7a x86 mips)
export PLATFORMS=(arm64-v8a)
#export CC=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-gcc
#export LD=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-ld
#export CPP=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-cpp
#export CXX=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-g++
#export AS=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-as
#export AR=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-ar
#export RANLIB=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-ranlib
export SYSROOT="$NDK_ROOT/platforms/$TARGET/$ARCH"
echo 'SYSROOT:'$SYSROOT
export CC=$(./ndk-which gcc $PLATFORMS) 
echo 'CC:' $CC
export LD=$(./ndk-which ld $PLATFORMS)
echo 'LD:' $LD
export CPP=$(./ndk-which cpp $PLATFORMS)
echo 'CPP:' $CPP
export CXX=$(./ndk-which g++ $PLATFORMS)
echo 'CXX:' $CXX
export AS=$(./ndk-which as $PLATFORMS)
echo 'AS:' $AS
export AR=$(./ndk-which ar $PLATFORMS)
echo 'AR:' $AR
export RANLIB=$(./ndk-which ranlib $PLATFORMS)
echo 'RANLIB:' $RANLIB

real_path() {
  [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

#Change this env variable to the number of processors you have
if [ -f /proc/cpuinfo ]; then
  JOBS=$(grep flags /proc/cpuinfo |wc -l)
elif [ ! -z $(which sysctl) ]; then
  JOBS=$(sysctl -n hw.ncpu)
else
  JOBS=2
fi

REL_SCRIPT_PATH="$(dirname $0)"
SCRIPTPATH=$(real_path $REL_SCRIPT_PATH)
CURLPATH="$SCRIPTPATH/../curl"
SSLPATH="$SCRIPTPATH/../openssl"

if [ -z "$NDK_ROOT" ]; then
  echo "Please set your NDK_ROOT environment variable first"
  exit 1
fi

if [[ "$NDK_ROOT" == .* ]]; then
  echo "Please set your NDK_ROOT to an absolute path"
  exit 1
fi

#Configure OpenSSL
#cd $SSLPATH
#./Configure android no-asm no-shared no-cast no-idea no-camellia no-whirpool
#EXITCODE=$?
#if [ $EXITCODE -ne 0 ]; then
#	echo "Error running the ssl configure program"
#	cd $PWD
#	exit $EXITCODE
#fi

#Build static libssl and libcrypto, required for cURL's configure
#cd $SCRIPTPATH
#$NDK_ROOT/ndk-build -j$JOBS -C $SCRIPTPATH ssl crypto
#EXITCODE=$?
#if [ $EXITCODE -ne 0 ]; then
#	echo "Error building the libssl and libcrypto"
#	cd $PWD
#	exit $EXITCODE
#fi

#Configure cURL
cd $CURLPATH
if [ ! -x "$CURLPATH/configure" ]; then
	echo "Curl needs external tools to be compiled"
	echo "Make sure you have autoconf, automake and libtool installed"

	./buildconf

	EXITCODE=$?
	if [ $EXITCODE -ne 0 ]; then
		echo "Error running the buildconf program"
		cd $PWD
		exit $EXITCODE
	fi
fi

export SYSROOT="$NDK_ROOT/platforms/$TARGET/$ARCH"
export CPPFLAGS="-I$NDK_ROOT/platforms/$TARGET/$ARCH/usr/include -I$INCLUDEPTH --sysroot=$SYSROOT -I$SYSRT"

export LIBS="-lssl -lcrypto"
export LDFLAGS="-L$SCRIPTPATH/obj/local/armeabi -L$LIBPTH -L$SYSRT"

./configure --host=$TOOL --target=$TOOL \
            --with-ssl=$SYSRT/openssl \
            --with-sysroot=$SYSRT \
            --enable-shared \
            --disable-verbose \
            --enable-threaded-resolver \
            --enable-libgcc \
            --enable-ipv6 \
            --prefix=$MYHOME \
            --libdir=$LIBPTH \
            --includedir=$INCLUDEPTH

EXITCODE=$?
if [ $EXITCODE -ne 0 ]; then
  echo "Error running the configure program"
  cd $PWD
  exit $EXITCODE
fi

#Patch headers for 64-bit archs
cd "$CURLPATH/include/curl"
sed 's/#define CURL_SIZEOF_LONG 4/\
#ifdef __LP64__\
#define CURL_SIZEOF_LONG 8\
#else\
#define CURL_SIZEOF_LONG 4\
#endif/'< curlbuild.h > curlbuild.h.temp

mv curlbuild.h.temp curlbuild.h

#Build cURL
$NDK_ROOT/ndk-build -j$JOBS -C $SCRIPTPATH curl
EXITCODE=$?
if [ $EXITCODE -ne 0 ]; then
	echo "Error running the ndk-build program"
	exit $EXITCODE
fi

#Strip debug symbols and copy to the prebuilt folder

DESTDIR=$SCRIPTPATH/../prebuilt-with-ssl/android

for p in ${PLATFORMS[*]}; do
  mkdir -p $DESTDIR/$p
  STRIP=$($SCRIPTPATH/ndk-which strip $p)
  echo '$STRIP:'
  echo $STRIP

  SRC=$SCRIPTPATH/obj/local/$p/libcurl.so
  DEST=$DESTDIR/$p/libcurl.so

  if [ -z "$STRIP" ]; then
    echo "WARNING: Could not find 'strip' for $p"
    cp $SRC $DEST
  else
    $STRIP $SRC --strip-debug -o $DEST
  fi
done

#Copying cURL headers
cp -R $CURLPATH/include $DESTDIR/
rm $DESTDIR/include/curl/.gitignore

cd $PWD
exit 0
